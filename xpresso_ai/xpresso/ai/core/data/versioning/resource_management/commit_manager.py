from datetime import datetime

from xpresso.ai.core.data.versioning.resource \
    import PachydermResource
from xpresso.ai.core.data.versioning.resource_management.branch_manager \
    import BranchManager


class CommitManager(PachydermResource):
    """
    Manages pachyderm commits
    """
    COMMIT_INFO_BRANCH = "branch_name"

    def __init__(self, pachyderm_client):
        super().__init__()
        self.pachyderm_client = pachyderm_client
        self.branch_manager = BranchManager(self.pachyderm_client)

    def inspect(self, repo_name, commit_id):
        """
        fetches commit info from pachyderm cluster

        :param repo_name:
            name of the repo commit belongs to
        :param commit_id:
            id of the commit
        :return:
            dict with commit information
        """
        commit_info = self.pachyderm_client.inspect_commit(repo_name, commit_id)
        return self.filter_commit_info(commit_info)

    def list(self, repo_name, branch_name):
        """
        lists commits in a repo

        :param repo_name:
            name of the repo
        :param branch_name:
            name of the branch
        :return:
            list of commits
        """
        # check for both repo and branch is done in check_branch_existence
        self.branch_manager.check_branch_existence(repo_name, branch_name)

        commit_info = self.pachyderm_client.list_commit(repo_name)
        commit_list = []
        for commit_item in commit_info:
            commit_data = self.filter_commit_info(commit_item)
            if commit_data[self.COMMIT_INFO_BRANCH] == branch_name:
                commit_list.append(commit_data)

        return commit_list

    @staticmethod
    def filter_commit_info(commit_info_object):
        """
        takes CommitInfo object and outputs a user friendly output

        :param commit_info_object:
            CommitInfo object
        :return:
            commit information as a dict
        """
        commit_info = dict()
        commit_info["commit_id"] = commit_info_object.commit.id if commit_info_object.commit.id else '-'
        commit_info["repo_name"] = commit_info_object.commit.repo.name
        commit_info["description"] = commit_info_object.description
        commit_info["branch_name"] = commit_info_object.branch.name
        commit_info["started_at"] = datetime.utcfromtimestamp(
            commit_info_object.started.seconds).strftime("%A, %B %d %Y, %T")
        commit_info["finished_at"] = datetime.utcfromtimestamp(
            commit_info_object.finished.seconds).strftime("%A, %B %d %Y, %T")
        # TODO: Add Start and End time in next update
        return commit_info
